import Board from "./Board";
import IPlayer from "./IPlayer";
import IMove from "./IMove";
import { GameStatus } from "./GameStatus";
import { CompletionState } from "./CompletionState";

export { GameStatus };

export class Game {
  readonly board: Board;
  status: GameStatus = GameStatus.InProgress;
  #completionState: CompletionState = CompletionState.Incomplete;

  #currentPlayerIndex: number = 0;
  #players: IPlayer[] = [];
  #moves: IMove[] = [];

  #rows: number[][] = [];
  #columns: number[][] = [];
  #diagonal1: number[] = [];
  #diagonal2: number[] = [];

  constructor(private size: number) {
    if (size <= 0) throw "Board size should be a positive number";
    this.board = new Board(size);

    for (var i = 0; i < size; i++) {
      this.#rows[i] = [];
      this.#columns[i] = [];
    }
  }

  private validatePlayer(player: IPlayer): void {
    if (!player) throw "Player cannot be null or undefined";
    if (!player.name.trim) throw "Player's name cannot be empty";
    if (!player.sign.trim || player.sign.length > 1)
      throw "Player's sign should be exactly 1 character";
  }

  addPlayer(player: IPlayer) {
    this.validatePlayer(player);
    this.#players.push(player);

    // Add player counters for each row, column and diagonal for efficient victory indication
    this.#diagonal1.push(0);
    this.#diagonal2.push(0);
    for (let i = 0; i < this.size; i++) {
      this.#rows[i].push(0);
      this.#columns[i].push(0);
    }
  }

  printSummary() {
    console.log(
      this.status === GameStatus.InProgress
        ? "Game is in progress"
        : this.#completionState === CompletionState.Tie
        ? "Game finished with tie"
        : `${this.#players[this.#currentPlayerIndex].name} won!`
    );
    for (const move of this.#moves) {
      const player = this.#players[move.playerIndex];
      console.log(
        `${player.name}:\t[${move.row} , ${move.column}] => ${player.sign}`
      );
    }
  }

  nextMove(row: number, column: number): boolean {
    if (this.status === GameStatus.Completed) return false; // Do nothing if game is already completed
    if (!this.#players.length) throw "No players are in the game";

    const currentPlayer = this.#players[this.#currentPlayerIndex];
    if (this.board.setPosition(row, column, currentPlayer.sign)) {
      this.#moves.push({
        playerIndex: this.#currentPlayerIndex,
        row: row,
        column: column,
      });

      // Update current player counters
      this.#rows[row][this.#currentPlayerIndex]++;
      this.#columns[column][this.#currentPlayerIndex]++;
      if (row === column) this.#diagonal1[this.#currentPlayerIndex]++;
      if (row === this.size - column - 1)
        this.#diagonal2[this.#currentPlayerIndex]++;

      // Check if current player won (in O(1))
      if (this.currentMoveWon(row, column)) {
        this.status = GameStatus.Completed;
        this.#completionState = CompletionState.Victory;
      } else {
        // Check for tie
        if (this.board.openSquaresCounter > 0) {
          // Open squares left- game continues
          this.#currentPlayerIndex =
            (this.#currentPlayerIndex + 1) % this.#players.length;
        } else {
          this.status = GameStatus.Completed;
          this.#completionState = CompletionState.Tie;
        }
      }
      return true;
    }
    return false;
  }

  private currentMoveWon(row: number, column: number): boolean {
    return (
      this.#rows[row][this.#currentPlayerIndex] === this.size || // Completed full row
      this.#columns[column][this.#currentPlayerIndex] === this.size || // Completed full column
      this.#diagonal1[this.#currentPlayerIndex] === this.size || // Completed full diagonal 1
      this.#diagonal2[this.#currentPlayerIndex] === this.size // Completed full diagonal 2
    );
  }
}
