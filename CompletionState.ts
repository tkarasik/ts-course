export enum CompletionState {
    Incomplete,
    Victory,
    Tie
}