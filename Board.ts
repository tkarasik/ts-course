export default class Board {
  #board: string[][] = [];
  openSquaresCounter: number;

  constructor(private size: number) {
    this.openSquaresCounter = size ** 2;
    this.#board = [];
    for (var i = 0; i < size; i++) {
      this.#board[i] = [];
      for (var j = 0; j < size; j++) this.#board[i][j] = " ";
    }
  }

  setPosition(row: number, column: number, sign: string): boolean {
    if (row < 0 || row >= this.size || column < 0 || column > this.size)
      throw "Position has to be within the board's boundaries";
    if (this.#board[row][column].trim()) return false; // Occupied

    this.#board[row][column] = sign;
    this.openSquaresCounter--;
    return true;
  }

  print(): void {
    for (let i = 0; i < this.size; i++)
      console.log(
        "%c%s",
        "color: black; background-color: white;",
        this.#board[i].join("|")
      );
  }
}
