export default interface IMove {
    playerIndex: number,
    row: number,
    column: number
}